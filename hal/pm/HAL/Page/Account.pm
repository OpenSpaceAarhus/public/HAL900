#-*-perl-*-
package HAL::Page::Account;
use strict;
use warnings;
use utf8;

use Data::Dumper;
use HTML::Entities;
use Digest::SHA qw(sha1_hex);
use File::Slurp qw(slurp);

use HAL;
use HAL::Pages;
use HAL::Session;
use HAL::Util;
use HAL::Email;
use HAL::Budget;

sub indexPage {
    my ($r, $q, $p) = @_;

    my $uRes = db->sql("select membertype.doorAccess, memberType, monthlyFee, username, email, phone, realname, smail, member.doorAccess, adminAccess " .
        "from member, membertype where member.membertype_id=membertype.id and member.id=?",
        getSession->{member_id});
    my ($memberDoorAccess, $memberType, $monthlyFee, $username, $email, $phone, $realname, $smail, $doorAccess, $adminAccess) = $uRes->fetchrow_array;
    $uRes->finish;

    $smail =~ s/[\n\r]+/<br>/g;

    my $html = qq'
<table style="width: 100%"><tr><td>

<div class="floaty" style="width:25%">
<h2>Detaljer [<a href="/hal/account/details">Ret</a>]</h2>
<p>
<strong>ID:</strong> $username<br>
$realname<br>
$smail<br>
Tlf. $phone
</p>
</div>

<div class="floaty" style="width:25%">
<h2>E-mail [<a href="/hal/account/email">Ret</a>]</h2>
<p>$email</p>
</div>

<div class="floaty" style="width:25%">
<h2>Medlemstype [<a href="/hal/account/type">Ret</a>]</h2>
<p>$memberType ($monthlyFee kr/måned)</p>
</div>

<div class="floaty" style="width:25%">
<h2>Privilegier</h2>
<ul style="list-style: none;">
';

    my $okCount = 0;
    my $noPin = 0;
    my $rr = db->sql("select id, rfid, pin, lost, name from rfid where owner_id=?", getSession->{member_id})
        or die "Failed to fetch list of RFIDs for user";
    while (my ($id, $rfid, $pin, $lost, $name) = $rr->fetchrow_array) {

        my $status = '';
        if ($lost) {
            $status = qq'Tabt &#10068;';
        } elsif ($pin) {
            $status = qq'OK &#9989;';
            $okCount++;
        } else {
            $status = qq'Mangler PIN-kode &#10060;';
            $noPin = qq'/hal/account/rfid/$id';
        }

        my $nn = $name // $rfid;
        $html .= qq'<li>&#128273; RFID-nøglebrik <a href="/hal/account/rfid/$id">$nn [$status]</a></li>';
    }
    $rr->finish;

    if ($doorAccess) {

        if ($okCount) {
            $html .= '<li>&#9989; Du kan låse døren til lokalerne op med din RFID-nøglebrik og PIN-kode</li>';

        } elsif ($noPin) {
            $html .= qq'<li>&#9940; Du kunne låse døren til lokalerne op med din RFID-nøglebrik, men du mangler <a href="$noPin">at vælge en PIN-kode</a>.</li>';

        } else {
            $html .= '<li>&#9940; Du kunne låse døren til lokalerne op, hvis ellers du havde en RFID-nøglebrik. Kontakt <a href="mailto:kasserer@osaa.dk">kasserer@osaa.dk</a></li>';
        }

    } elsif ($memberDoorAccess) {
        $html .= '<li>&#9940; Du kan ikke låse døren til lokalerne op. Kontakt <a href="mailto:kasserer@osaa.dk">kasserer@osaa.dk</a></li>';

    } else {
        $html .= '<li>&#9940; Du kan ikke låse døren til lokalerne op. <a href="/hal/account/type">Opgrader til betalende medlem</a></li>';
    }

    $html .= $adminAccess ? '<li>&#9989; Du kan administrere systemet</li>' : '';
    $html .= "</ul></div>\n";

    my $currentExternal = HAL::Page::AccountExternalIds::currentExternalIds()->{html};
    $html .= qq!
        <div class="floaty">
        <h2>Eksterne medlemskaber [<a href="/hal/account/external">Ret</a>]</h2>
        $currentExternal
        </div>
    !;

    $html .= "<td></tr></table> <!-- Yes I'm using a table for layout, so sue me! -->";

    if ($doorAccess) {
        my $mf = HALRoot() . "/.hal/paying-member-message.html";
        if (-f $mf) {
            $html .= slurp($mf, binmode => ':utf8');
        }
    }

    $html .= "<h2>Kontingent</h2>";
    $html .= "<p>";
    if ($monthlyFee > 0) {
        $html .= "<span style='font-size:2em;'>&#9989;</span> Du er p.t. registreret som <strong>$memberType</strong>. Dit kontingent er <strong>$monthlyFee</strong> kr pr. måned.";
    } else {
        $html .= "<span style='font-size:2em;'>&#9888;&#65039;</span> Du er lige nu <em>ikke</em> registreret som betalende medlem af OSAA.</p> ";
    }
    $html .= " Du kan ændre dette under <a href='type'>Medlemstype</a>.";
    $html .= "</p>";
    $html .= "<p>Open Space Aarhus’ kontooplysninger er:
<div style='padding: 1.5em;font-size:large;'>
Reg.nr.: <span style='font-size:x-large;margin-right:2em;background-color:#ccc;padding:0.2em;border-radius:0.4em;'>2316</span>
Kontonr.: <span style='font-size:x-large;background-color:#ccc;padding:0.2em;border-radius:0.4em;'>0741891514</span>
</div>
";
    $html .= "<details><summary>Kontingentbetaling FAQ</summary>";
	$html .= "<dl class='faq'>";
	$html .= "<dt>Hvordan får jeg egen adgang til spacet?</dt>";
	$html .= "<dd><ol>";
	$html .= "<li>Du opretter dig som medlem i HAL (det har du gjort, det er der du er nu)</li>";
	$html .= "<li>Du skifter din <a href='type'>Medlemstype</a> til en, der giver adgang til spacet</li>";
	$html .= "<li>Du betaler dit kontingent (se nedenfor)</li>";
	$html .= "<li>Du laver en aftale med et bestyrelsesmedlem om udlevering af nøglebrik</li>";
	$html .= "</ol></dd>";
	$html .= "<dt>Hvordan betaler jeg kontingent?</dt>";
	$html .= "<dd>Betaling af kontingent foregår ved at lave en bankoverførsel med din e-mail adresse (<strong>$email</strong>) i kommentarfeltet, så vi har pengene senest den første i hver måned.";
    if ($monthlyFee <= 0) {
        $html .= "<p>Bemærk, at det <em>ikke</em> er tilstrækkeligt at lave en overførsel til kontoen for at blive registreret som medlem. Du skal <em>også</em> rette din <a href='type'>medlemstype</a> hvis du ønsker at bliver betalende medlem.";
    }
	$html .= "</dd>";
	$html .= "<dt>Hvor meget og hvor tit skal jeg overføre kontingent?</dt>";
	$html .= "<dd>Det er naturligvis en afvejning. Vi må ikke være bank for folk, så som udgangspunkt må du ikke have mere stående, end hvad der svarer til et års kontingent. ";
	$html .= "De fleste overfører et sted mellem 3 og 6 måneders kontingent af gangen. ";
	$html .= "</dd>";
	$html .= "<dt>Er der nogle muligheder for nedsat kontingent?</dt> ";
	$html .= "<dd>Ja, vi giver halv pris på det normale kontingent for fuldtidsstuderende. ";
	$html .= "Derudover har vi en ordning med PROSA, hvor de betaler halvdelen af dit (normale) kontingent, hvis dit medlemsnummer er <a href='external'>registreret korrekt</a>. ";
	$html .= "</dd>";
	$html .= "<dt>Hvor kan jeg læse mere om de forskellige medlemstyper?</dt>";
	$html .= "<dd>Du kan læse mere på vores <a href='https://wiki.osaa.dk/wiki/MedlemsTyper' target='_blank'>wikiside om medlemstyper</a></dd>";
	$html .= "</dl>";
	$html .= "</details>";

    my $ar = db->sql("select account.id, accountName, typeName, type_id " .
        "from account inner join accounttype on (type_id=accounttype.id) " .
        "where owner_id=? order by accounttype.id",
        getSession->{member_id})
        or die "Failed to look up accounts owned by the user";

    while (my ($account_id, $accountName, $typeName, $type_id) = $ar->fetchrow_array) {
        my @table;
        my $tx = db->sql("select t.id, to_char(t.created, 'YYYY-MM-DD HH24:MI'), source_account_id, sa.accountName, target_account_id, ta.accountName, amount, comment " .
            "from accountTransaction t " .
            "inner join account sa on (t.source_account_id = sa.id) " .
            "inner join account ta on (t.target_account_id = ta.id) " .
            "where target_account_id=? or source_account_id=? " .
            "order by t.id asc", $account_id, $account_id);
        my $sum = 0;
        my $sumIn = 0;
        my $sumOut = 0;
        while (my ($tid, $created, $source_id, $source, $target_id, $target, $amount, $comment) = $tx->fetchrow_array) {
            my $other = $source_id == $account_id ? $target : $source;
            my $delta = $source_id == $account_id ? $amount : -$amount;

            $sum += $delta;

            push @table, [ "<td style='white-space: nowrap;'>$tid</td>",
                "<td>$created</td>",
                "<td>$comment</td>",
                "<td>$other</td>",
                sprintf("<td style='text-align:right;background-color:%s'>%0.2f</td>", ($delta < 0 ? "#ffc6b3;" : "#ccffcc"), $delta),
                sprintf("<td style='text-align:right;background-color:%s'>%0.2f</td>", ($sum < 0 ? "#ffc6b3;" : "#ccffcc"), $sum)
            ];
        }
        $tx->finish;

        @table = reverse @table;

        my $saldo = sprintf("%0.2f", $sum);

        $html .= qq'<h2>$typeName: $accountName - Saldo: $saldo</h2>';
        $html .= "<table><th>ID</th><th>Dato</th><th>Transaktion</th><th>Fra/Til konto</th><th>Beløb</th><th>Saldo</th>\n";
        my $count = 0;
        for my $r (@table) {
            my $class = ($count++ & 1) ? 'class="odd"' : 'class="even"';
            $html .= qq'<tr $class>' . join('', @$r) . qq'</tr>\n';
        }
        $html .= "</table>";
    }
    $ar->finish;

    return outputAccountPage('index', 'Oversigt', $html);
}

sub logoutPage {
    my ($r, $q, $p) = @_;

    logoutSession();
    return outputGoto('/hal/');
}

sub emailPage {
    my ($r, $q, $p) = @_;

    my $html = '';

    $html = qq'<form method="POST" action="/hal/account/email">';

    my $errors = 0;

    $html .= emailInput(
        "E-mail",
        "<p>Indtast din nye e-mailadresse. </p> <p>Vi sender en mail til e-mailadressen med et link, som du skal klikke på for at fortsætte. </p>",
        'email',
        $p,
        sub {
            my ($v, $p, $name) = @_;
            if (length($v) < 2) {
                $errors++;
                return "Din e-mail adresse kan da umuligt være så kort";
            }

            my $res = db->sql("select count(*) from member where email = ?", $p->{email});
            my ($inuse) = $res->fetchrow_array;
            $res->finish;
            my $ue = escape_url($p->{email});

            #system("/home/ff/test 1>&2");
            if ($inuse) {
                $errors++;
                return qq'E-mailadressen er allerede i brug.';

            } elsif (!validateEmail($p->{email})) {
                $errors++;
                return qq'E-mailadressen er ugyldig, prøv igen.';
            }

            return "";
        });

    $html .= '
<hr>
<input type="submit" name="gogogo" value="Skift til denne e-mailadresse!">
</form>';

    if ($p->{gogogo}) {
        if ($errors) {
            $errors = "en" if $errors == 1;
            $html .= "<p>Hovsa, der er $errors fejl!</p>";

        } else {
            my $uRes = (db->sql('select username,email,passwd from member where id=?', getSession->{member_id}));
            my ($userName, $oldMail, $passwd) = $uRes->fetchrow_array;
            $uRes->finish;

            my $key = sha1_hex($p->{email} . $passwd);
            my $ue = escape_url($p->{email});
            my $uu = escape_url($userName);

            my $email = sendmail('changeemail@hal.osaa.dk', $p->{email},
                'Skift af e-mail for dit Open Space Aarhus medlemskab',
                "En eller anden, måske dig, har bedt om at skifte din e-mail adresse fra $oldMail til $p->{email}.
Hvis du ønsker at skifte til den nye adresse, klik her:
https://hal.osaa.dk/hal/account/confirmemail?user=$uu&email=$ue&key=$key&ex=44

Hvis det ikke er dig, der har bedt om at skifte mail adresse, så har nogen fået adgang til din
konto, og du bør logge ind og skifte dit password - lige nu - og kontakte bestyrelsen\@osaa.dk.
"
            );
            l "Email-change: $oldMail -> $p->{email}: https://hal.osaa.dk/hal/account/confirmemail?user=$uu&email=$ue&key=$key&ex=44";

            $html .= "<p>Nu er der blevet sent en mail til dig med et link i, klik på linket for at skifte e-mailadresse.</p>";
        }
    }

    return outputAccountPage('email', 'Skift email', $html);
}

sub emailConfirmPage {
    my ($r, $q, $p) = @_;

    my $uRes = (db->sql('select id,email,passwd from member where username=?', $p->{user}));
    my ($id, $oldMail, $passwd) = $uRes->fetchrow_array;
    $uRes->finish;

    if ($id != getSession->{member_id}) {
        logoutSession();
        getSession()->{wanted} = $r->unparsed_uri;
        return outputGoto("/hal/login");
    }

    my $key = sha1_hex($p->{email} . $passwd);
    if ($key ne $p->{key}) {
        l "Invalid key for email change from $oldMail to $p->{email}";
        return outputGoto("/hal/account");
    } elsif ($p->{gogogo} and $p->{doit}) {

        if (db->sql('update member set email=? where id=?', $p->{email}, $id)) {

            return outputGoto('/hal/account/');
        } else {
            l "Failed to change email address";
            return outputGoto('/hal/account/');
        }
    }

    my $html = '<form method="POST" action="/hal/account/confirmemail">';

    $html .= encode_hidden({
        email => $p->{email},
        key   => $p->{key},
        user  => $p->{user},
    });

    $html .= qq'
<p>
For at skifte din mailadresse sæt kryds her og tryk på "skift min mail" knappen.
</p>

<input type="checkbox" value="18" name="doit">Skift min e-mail adresse fra $oldMail til <strong>$p->{email}</strong>

<hr>
<input type="submit" name="gogogo" value="Skift til denne e-mail adresse!">
</form>';

    return outputAccountPage('email', 'Skift email', $html);
}

sub passwdPage {
    my ($r, $q, $p) = @_;

    my $uRes = (db->sql('select passwd,username from member where id=?', getSession->{member_id}));
    my ($passwd, $username) = $uRes->fetchrow_array;
    $uRes->finish;

    my $key = sha1_hex($passwd);

    my $form = qq'
<p>
	Denne side kan skifte dit password.
 </p>
 <p>
 	Hvis du ikke ønsker at skifte dit password skal du blot forlade siden.
</p>
<form method="POST" action="/hal/account/passwd">';
    $form .= encode_hidden({
        key => $key,
    });

    my $errors = 0;
    $form .= passwdInput2("Password",
        "Dit password som skal give dig adgang til dette system.",
        'passwd', $p, sub {
            my ($v, $p, $name) = @_;
            if (length($v) < 6) {
                $errors++;
                return "Dit password skal være mindst 6 tegn langt";
            }
            if ($v !~ /[A-ZÆØÅ]/) {
                $errors++;
                return "Dit password skal indeholde mindst et stort bogstav";
            }
            if ($v !~ /[a-zæøå]/) {
                $errors++;
                return "Dit password skal indeholde mindst et lille bogstav";
            }
            if ($v !~ /\d/) {
                $errors++;
                return "Dit password skal indeholde mindst et tal";
            }
            if (index(lc($v), lc($username)) >= 0) {
                $errors++;
                return "Dit password må ikke indeholde dit brugernavn";
            }
            if (index(lc($v), 'osaa') >= 0) {
                $errors++;
                return "Dit password må ikke indeholde osaa";
            }
            if ($v ne $p->{"${name}_confirm"}) {
                $errors++;
                return "De to passwords skal være ens";
            }

            return "";
        });

    $form .= '
<hr>
<input type="submit" name="gogogo" value="Skift mit password!">
</form>';

    if ($p->{gogogo} and $key eq $p->{key}) {
        if ($errors) {
            $errors = "en" if $errors == 1;
            $form .= "<p>Hovsa, der er $errors fejl!</p>";
        } else {
            if (db->sql('update member set passwd=? where id=?', passwordHash($p->{passwd}), getSession->{member_id})) {
                $form = "<p>Dit password er blevet skiftet.</p>";
            } else {
                $form .= "<p>Hovsa, noget gik galt, prøv igen.</p>";
            }
        }
    }

    return outputAccountPage('passwd', 'Skift Password', $form);
}

sub mtRadio {
    my ($types, $id, $current) = @_;

    return '<td></td>' unless defined $id;
    my $type = $types->{$id};

    my $checked = defined $current && $type->{id} == $current ? ' checked' : '';

    return qq'<td title="$type->{name}">' .
        qq'<label><input type="radio" name="membertype" value="$type->{id}"$checked>$type->{fee},-</input></label>' .
        qq'</td>'

}

sub typePage {
    my ($r, $q, $p) = @_;

    my $cb = currentBudget();

    my ($memberCount, $breakevenFee) = ($cb->{memberCount}, $cb->{breakevenFee});

    my $uRes = (db->sql('select membertype_id from member where id=?', getSession->{member_id}));
    my ($membertype_id) = $uRes->fetchrow_array;
    $uRes->finish;

    my %types;
    my $typesRes = db->sql('select id, memberType, monthlyFee, doorAccess from memberType where userpick order by id');
    while (my ($id, $memberType, $monthlyFee, $doorAccess) = $typesRes->fetchrow_array) {
        $types{$id} = {
            id   => $id,
            key  => $id,
            fee  => int($monthlyFee),
            da   => $doorAccess,
            mt   => $memberType,
            name => "$memberType ($monthlyFee kr/måned) " . ($doorAccess ? '- Inkluderer nøgle til lokalerne' : '- Uden nøgle til lokalerne'),
        }
    }
    $typesRes->finish;

    my $tableRes = db->sql('select * from memberType_group mtg join memberType mt on mt.id = mtg.normal_id order by mtg.id ');
    my @groups;
    while (my $row = $tableRes->fetchrow_hashref) {
        push @groups, $row;
    }
    $tableRes->finish;

    $p->{membertype} ||= $membertype_id;

    my $errors = 0;
    my $form = qq'<p>Se <a href="https://wiki.osaa.dk/index.php?title=MedlemsTyper">MedlemsTyper på wiki</a> for en komplet beskrivelse af typerne.</p>
<table><tr><th>Type</th></th><th style="padding:0pt 4pt;">Normaltakst</th><th style="padding:0pt 4pt;">Studie-takst</th><th>Beskrivelse</th></tr>
    <form method="POST" action="/hal/account/type">';

    for my $row (@groups) {
        my $norm = mtRadio(\%types, $row->{normal_id}, $p->{membertype});
        my $half = mtRadio(\%types, $row->{half_id}, $p->{membertype});
        $form .= qq'<tr style="$row->{css_style}"><td>$row->{membertype}$norm$half<td>$row->{description}</td></tr>\n';
    }

    $errors++ unless $p->{membertype};
    $errors++ unless $types{$p->{membertype}}; # Validate that the type is pickable

    $form .= '
    </table>
<hr>
<input type="submit" name="gogogo" value="Skift min medlemstype!">
</form>
';

    if ($p->{membertype}) {
        my $t = $types{$p->{membertype}};
        if ($t) {
            if ($t->{fee} >= $breakevenFee) {
                $form .= qq'<p>Du har valgt at betale $t->{fee} kr/måned i kontingent, det er super fedt, tak!</p>';
            }
        }
    }

    if ($p->{gogogo}) {
        if ($errors) {
            $form .= "<p>Hovsa, vælg venligst hvilken type medlemskab du ønsker!</p>";
        } else {
            if (db->sql('update member set membertype_id=? where id=?',
                $p->{membertype}, getSession->{member_id})) {

                return outputGoto('/hal/account');
            } else {
                $form .= "<p>Hovsa, noget gik galt, prøv igen.</p>";
            }
        }
    }

    return outputAccountPage('type', 'Skift Medlemstype', $form);
}

sub detailsPage {
    my ($r, $q, $p) = @_;

    my $form = '<form method="POST" action="/hal/account/details">';

    my $uRes = (db->sql('select realname, smail, phone from member where id=?', getSession->{member_id}));
    my ($realname, $smail, $phone) = $uRes->fetchrow_array;
    $uRes->finish;

    my $errors = 0;

    $p->{name} ||= $realname;
    $form .= textInput("Fulde navn", "Dit rigtige navn, incl. evt. mellemnavne og efternavn", 'name', $p, sub {
        my ($v, $p, $name) = @_;
        if (length($v) < 4) {
            $errors++;
            return "Dit fulde navn kan umuligt være mindre end 4 tegn langt.";
        }
        if ($v !~ /^[\pL .-]+$/) {
            $errors++;
            return "Æh, hvad?";
        }
        if ($v !~ / /) {
            $errors++;
            return "Også efternavnet, tak.";
        }

        return "";
    });

    $p->{snailmail} ||= $smail;
    $p->{snailmail} =~ s/\s+$//s;
    $form .= areaInput("Snailmail", "Din postadresse, incl. gade, husnummer, by og postnummer", 'snailmail', $p, sub {
        my ($v, $p, $name) = @_;
        if (length($v) < 4) {
            $errors++;
            return "Din adresse kan umuligt være mindre end 4 tegn lang.";
        }
        my @lines = split /\s*\n\s*/, $v;
        if (@lines < 2) {
            $errors++;
            return "Skriv venligst post adressen som den normalt står på et brev.";
        }

        return "";
    });

    $p->{phone} ||= $phone;
    $form .= telInput("Telefonnummer", "Dit telefonnummer, som du helst vil kontaktes af OSAA på", 'phone', $p, sub {
        my ($v, $p, $name) = @_;
        $v =~ s/[^+\d]+//g;
        if (length($v) < 8) {
            $errors++;
            return "Dit telefonnummer kan umuligt være kortere end 4 tal langt.";
        }
        return "";
    });

    $form .= '
<hr>
<input type="submit" name="gogogo" value="Gem mine oplysninger">
</form>';

    if ($p->{gogogo}) {
        if ($errors) {
            $errors = "en" if $errors == 1;
            $form .= "<p>Hovsa, der er $errors fejl!</p>";
        } else {
            if (db->sql('update member set realname=?, smail=?, phone=? where id=?',
                $p->{name}, $p->{snailmail}, $p->{phone}, getSession->{member_id})) {
                l "Updated: name=$p->{name}, snailmail=$p->{snailmail}, phone=$p->{phone}";
                return outputGoto('/hal/account');
            } else {
                $form .= "<p>Hovsa, noget gik galt, prøv igen.</p>";
            }
        }
    }

    return outputAccountPage('details', 'Ret bruger oplysninger', $form);
}

sub rfidPage {
    my ($r, $q, $p, $rfid_id) = @_;

    if ($p->{lost}) {
        db->sql('update rfid set pin=null, lost=true where id=? and owner_id=?',
            $rfid_id, getSession->{member_id}) or die "Urgh";
        l "Marked rfid as lost rfid_id=$rfid_id";
    }
    if ($p->{found}) {
        db->sql('update rfid set pin=null, lost=false where id=? and owner_id=?',
            $rfid_id, getSession->{member_id}) or die "Urgh";
        l "Marked rfid as lost rfid_id=$rfid_id";
    }

    my $rr = db->sql("select rfid, pin, lost, name from rfid where owner_id=? and id=?",
        getSession->{member_id}, $rfid_id)
        or die "Failed to fetch RFID for user $rfid_id";
    my ($rfid, $pin, $lost, $name) = $rr->fetchrow_array;
    $rr->finish;
    return outputGoto('/hal/account') unless $rfid;

    my $html = '';

    if ($rfid ne $name) {
        $html .= "<p>Din RFID-nøglebrik har nummer <strong>$rfid</strong> og hedder <strong>$name</strong>.</p>";
    } else {
        $html .= "<p>Din RFID-nøglebrik har nummer <strong>$rfid</strong>.</p>";
    }

    if ($lost) {
        $html .= qq'<p>Denne RFID-nøglebrik kan ikke bruges, fordi den er markeret som tabt. Hvis du har fundet den, så klik her: <a href="/hal/account/rfid/$rfid_id?found=1">Fundet!</a></p>';

    } else {
        if (!$pin) {
            $html .= qq'<p>Denne RFID-nøglebrik kan ikke bruges, fordi den ikke har nogen PIN-kode.</p>';
        }

        $html .= qq'<form method="POST" action="/hal/account/rfid/$rfid_id">';

        my $errors = 0;
        $html .= passwdInput2("PIN-kode",
            "PIN-koden gør det muligt at bruge RFID-nøglebrikken. Den skal være mindst 5 cifre lang, og må ikke være den samme som du bruger andre steder f.eks. på kredit kort.",
            'pin', $p, sub {
                my ($v, $p, $name) = @_;
                if ($v !~ /^\d+$/) {
                    $errors++;
                    return "Din PIN-kode må kun indeholde tal";
                }
                if ($v =~ /^0/) {
                    $errors++;
                    return "Din PIN-kode må ikke starte med 0";
                }
                if (length($v) < 5) {
                    $errors++;
                    return "Din PIN-kode skal være mindst 5 cifre lang";
                }
                if ($v =~ /(.+)\1/) {
                    $errors++;
                    return "Din PIN-kode må ikke indeholde gentagelser";
                }

                my @v = split(//, $v);
                my $oc = -1;
                my $monoCount = 0;
                my $slope = 0;
                for my $c (@v) {
                    my $ns = $c - $oc;
                    $oc = $c;
                    if ($ns == $slope and abs($slope) == 1) {
                        $monoCount++;
                    } else {
                        $monoCount = 0;
                    }
                    $slope = $ns;

                    if ($monoCount >= 2) {
                        $errors++;
                        return "Cifrene må ikke komme i rækkefølge";
                    }
                }

                if ($v ne $p->{"${name}_confirm"}) {
                    $errors++;
                    return "De to PIN-koder skal være ens";
                }

                return "";
            });

        $html .= '
<hr>
<input type="submit" name="gogogo" value="Skift min PIN-kode!">
</form>';

        # TODO: Add name editor

        if ($p->{gogogo}) {
            if ($errors) {
                $html .= "<p>Hovsa, der er noget galt, prøv igen!</p>";
            } else {
                if (db->sql('update rfid set pin=? where id=? and owner_id=?',
                    $p->{pin}, $rfid_id, getSession->{member_id})) {
                    l "Updated PIN for: rfid_id=$rfid_id";
                    $html .= "<p>Din PIN-kode er nu opdateret i HAL. Den vil blive opdateret i dørlåsen hurtigst muligt. Dette sker normalt indenfor få minutter.</p>";
                } else {
                    $html .= "<p>Hovsa, noget gik galt, prøv igen.</p>";
                }
            }
        }

        $html .= qq'<h2>Glemt PIN-kode?</h2><p>Hvis du glemmer din PIN-kode til din RFID-nøglebrik, kan du altid bruge denne side til at vælge en ny kode.</p>';

        $html .= qq'<h2>Tabt nøgle?</h2><p>Hvis du har tabt RFID-nøglebrikken, kontakt <a href="mailto:kasserer\@osaa.dk">kasserer\@osaa.dk</a> så den kan blive markeret som tabt eller <a href="/hal/account/rfid/$rfid_id?lost=1">klik her for at markere den som tabt</a>. Hvis du finder nøglebrikken igen, kan du nemt markere den som fundet på denne side.</p>';
    }

    return outputAccountPage('rfid', 'Ret RFID-nøglebrik', $html);
}

sub rfidsPage {
    my ($r, $q, $p) = @_;

    my $html = '';

    my $list = '';
    my $lastId = 0;
    my $count = 0;
    my $rr = db->sql("select id, rfid, pin, lost, name from rfid where owner_id=? order by id", getSession->{member_id})
        or die "Failed to fetch list of RFIDs for user";
    while (my ($id, $rfid, $pin, $lost, $name) = $rr->fetchrow_array) {

        my $status = '';
        if ($lost) {
            $status = qq'Tabt';
        } elsif ($pin) {
            $status = qq'OK';
        } else {
            $status = qq'Mangler PIN-kode';
        }

        $list .= qq'<li><a href="/hal/account/rfid/$id">$name [$status]</a></li>';
        $lastId = $id;
        $count++;
    }
    $rr->finish;

    return outputGoto("/hal/account/rfid/$lastId") if $count == 1;

    if ($list) {

        $html .= qq'<p>OSAA bruger RFID-nøglebrikker med PIN-koder til at give adgang til lokalerne. Klik et link for at se detaljer.</p>';

        $html .= "<ul>$list</ul>";

    } else {
        my $uRes = db->sql("select membertype.doorAccess, memberType, monthlyFee, username, email, phone, realname, smail, member.doorAccess, adminAccess " .
            "from member, membertype where member.membertype_id=membertype.id and member.id=?",
            getSession->{member_id});
        my ($memberDoorAccess, $memberType, $monthlyFee, $username, $email, $phone, $realname, $smail, $doorAccess, $adminAccess) = $uRes->fetchrow_array;
        $uRes->finish;

        if ($memberDoorAccess) {
            $html .= qq'<p>Du har ingen registrerede RFID-nøglebrikker, kontakt <a href="mailto:kasserer\@osaa.dk">kasserer\@osaa.dk</a></p>';
        } else {
            $html .= qq'<p>Du har ingen registrerede RFID-nøglebrikker.</p>';
        }
    }

    return outputAccountPage('rfid', 'RFID-nøglebrikker', $html);
}

BEGIN {
    ensureLogin(qr'^/hal/account');
    addHandler(qr'^/hal/account/?$', \&indexPage);
    addHandler(qr'^/hal/account/logout$', \&logoutPage);
    addHandler(qr'^/hal/account/email$', \&emailPage);
    addHandler(qr'^/hal/account/passwd$', \&passwdPage);
    addHandler(qr'^/hal/account/type$', \&typePage);
    addHandler(qr'^/hal/account/details$', \&detailsPage);
    addHandler(qr'^/hal/account/confirmemail$', \&emailConfirmPage);
    addHandler(qr'^/hal/account/rfid/(\d+)$', \&rfidPage);
    addHandler(qr'^/hal/account/rfid$', \&rfidsPage);
}

42;
