#-*-perl-*-
package HAL::Page::AccessControl;
use strict;
use warnings;
use utf8;

use Data::Dumper;
use HTML::Entities;
use Digest::SHA qw(sha1_hex);
use POSIX;
use Text::vCard;
use Text::vCard::Addressbook;
#use GD::Barcode::QRcode;

use HAL;
use HAL::Pages;
use HAL::Session;
use HAL::Util;
use HAL::Email;
use HAL::TypeAhead;

sub getDevices {

    my $rb = db->sql("select id, name, disabled from access_device");
    my %ad;
    while (my ($id, $name, $disabled) = $rb->fetchrow_array) {

        $ad{$id} = {
            id       => $id,
            name     => $name,
            disabled => $disabled,
        };
    }
    $rb->finish;

    return %ad;
}

sub getGpios {

    my $rb = db->sql("select device_id, index, name, set_event, clear_event, output, inverted from gpio_bit");
    my %res;
    while (my ($device_id, $index, $name, $set_event, $clear_event, $output, $inverted) = $rb->fetchrow_array) {

        $res{$device_id}{$index} = {
            device_id   => $device_id,
            index       => $index,
            name        => $name,
            set_event   => $set_event,
            clear_event => $clear_event,
            output      => $output,
            inverted    => $inverted
        };
    }
    $rb->finish;

    return %res;
}

sub renderEvent {
    my ($gpio, $active) = @_;

    my $s = "<ul>";
    $s .= join("", map {
        "<li>$_</li>"
    } map {
        my $se = $_->{set_event} ? ": $_->{set_event}" : ": set";
        $_->{name} . $se;
    } grep {
        $active->{outputs} & (1 << $_->{index})
    } map {
        $gpio->{$_}
    } sort keys %$gpio);
    $s .= "</ul>";
    $s .= " from $active->{created} to $active->{expires}";

    return $s;
}

sub getActiveOutputs {

    my $rb = db->sql("select id, creator_id, device_id, created, expires, outputs from output_event where expires > now()");
    my %active;
    while (my ($id, $creatorId, $deviceId, $created, $expires, $outputs) = $rb->fetchrow_array) {

        $active{$deviceId} = {
            id        => $id,
            creatorId => $creatorId,
            deviceId  => $deviceId,
            created   => $created,
            expires   => $expires,
            outputs   => $outputs,
        };
    }
    $rb->finish;

    return %active;
}

sub accessCtrlPage {
    my ($r, $q, $p) = @_;

    my %active = getActiveOutputs();

    my %gpios = getGpios();

    my $html = "";

    my %devices = getDevices();
    for my $deviceId (sort grep {!$devices{$_}{disabled}} keys %devices) {
        my $device = $devices{$deviceId};
        my $gpio = $gpios{$deviceId};
        next unless $gpio;
        my $active = $active{$deviceId};

        if ($p->{deviceId} and $p->{deviceId} == $deviceId) {
            if ($p->{expire}) {
                db->sql("update output_event set expires=now() where id=?", $active->{id});
            }

            if ($p->{create}) {
                my $outputs = 0;
                for my $gpo (grep {$_->{output}} map {$gpio->{$_}} sort keys %$gpio) {
                    $outputs |= 1 << $gpo->{index} if $p->{"set-$gpo->{index}"};
                }
                if (!$outputs) {
                    $html .= "<p>Error: Please pick output(s) to turn on for $device->{name}</p>";
                }

                die "Need TTL!" unless $p->{ttl};

                db->sql("insert into output_event (creator_id, device_id, outputs, expires) values (?,?,?, now()+make_interval(mins=>?))",
                    getSession->{member_id}, $deviceId, $outputs, $p->{ttl});
            }

            %active = getActiveOutputs();
        }
    }

    for my $deviceId (sort grep {!$devices{$_}{disabled}} keys %devices) {
        my $device = $devices{$deviceId};
        my $gpio = $gpios{$deviceId};
        next unless $gpio;
        $html .= "<h3>$device->{name}</h3>\n";
        my $active = $active{$deviceId};

        $html .= qq'<form METHOD="POST"><input type="hidden" name="deviceId" value="$deviceId">\n';
        if ($active) {
            $html .= "<p>Currently active output: " . renderEvent($gpio, $active) .
                qq'<input type="submit" name="expire" value="Expire now"></p>';
        } else {

            $html .= "<ul>";
            for my $gpo (grep {$_->{output}} map {$gpio->{$_}} sort keys %$gpio) {
                if ($gpo->{inverted}) {
                    my $se = $gpo->{clear_event} ? ": $gpo->{clear_event}" : "";
                    $html .= qq'<li><input type="checkbox" name="set-$gpo->{index}" value="1">!$gpo->{name}$se</li>\n';
                } else {
                    my $se = $gpo->{set_event} ? ": $gpo->{set_event}" : "";
                    $html .= qq'<li><input type="checkbox" name="set-$gpo->{index}" value="1">$gpo->{name}$se</li>\n';
                }
            }
            $html .= "</ul>";

            $html .= qq'Expires in <input type="text" name="ttl" value="120"> minutes\n';
            $html .= qq'<input type="submit" name="create" value="Create">\n';
        }
        $html .= "</form>\n\n";
    }

    return outputAdminPage('accessctrl', 'Access Ctrl', $html);
}

BEGIN {
    addHandler(qr'^/hal/admin/accessctrl$', \&accessCtrlPage);
}

12;
