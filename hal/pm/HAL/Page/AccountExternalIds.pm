#-*-perl-*-
package HAL::Page::AccountExternalIds;

use strict;
use warnings;
use utf8;
use Data::Dumper;
use HTML::Entities;
use Digest::SHA qw(sha1_hex);
use File::Slurp qw(slurp);

use HAL;
use HAL::Pages;
use HAL::Session;
use HAL::Util;
use HAL::Email;
use HAL::Budget;

sub currentExternalIds() {

    my $count = 0;

    my $rr = db->sql("select o.id, name, description, name_of_external_id, value
                      from external_organization o
                      join external_member_id i on (i.external_organization_id=o.id)
                      where i.member_id=?
                      order by o.id",
        getSession->{member_id})
        or die "Failed to fetch list of memberships for user";

    my @ids;
    while (my ($id, $name, $description, $nameOfField, $value) = $rr->fetchrow_array) {
        $count++;

        push @ids, {
            id          => $id,
            name        => $name,
            description => $description,
            field       => $nameOfField,
            value       => $value,
        };
    }
    $rr->finish;

    my $html;

    if (@ids) {
        $html = "<ul>\n";
        for my $i (@ids) {
            $html .= "<li><span title='$i->{field}: $i->{value}'>$i->{name}</span></li>";
        }
        $html .= "</ul>\n";
    } else {
        $html = '<p>Der er ingen eksterne medlemskaber.</p>';
    }

    return {
        html => $html,
        ids  => \@ids,
    };
}

sub allExternalOrgs() {

    my $rr = db->sql("select id, name, description, name_of_external_id, validation_email
                      from external_organization o
                      order by id")
        or die "Failed to fetch list of memberships for user";

    my %orgById;
    while (my ($id, $name, $description, $nameOfField, $validationEmail) = $rr->fetchrow_array) {
        $orgById{$id} = {
            id          => $id,
            name        => $name,
            description => $description,
            field       => $nameOfField,
            email       => $validationEmail,
        };
    }
    $rr->finish;

    return \%orgById;
}

sub externalPage {
    my ($r, $q, $p) = @_;

    my $html = '<p>Denne side viser de medlemskaber af eksterne organisationer, som du har registreret lige nu.</p>';

    $html .= qq'<form method="GET" action="/hal/account/external/edit">';
    my @ids = @{currentExternalIds()->{ids}};
    my %currentIDs;
    if (@ids) {

        $html .= "<table><tr><th>Organisation</th><th>Medlemsnummer</th><th>Beskrivelse</th><th>Slet</th></tr>\n";
        my $count = 0;
        for my $i (@ids) {
            my $class = ($count++ & 1) ? 'class="odd"' : 'class="even"';
            $html .= qq'<tr $class>'
                . qq'<td><strong>$i->{name}</strong></td>'
                . qq'<td>$i->{value}</td>'
                . qq'<td>$i->{description}</td>'
                . qq'<td><button type="submit" name="delete" value="$i->{id}">Slet</button></td>'
                . qq'</tr>\n';
            $currentIDs{$i->{id}}++;
        }
        $html .= "</table>";
    } else {
        $html .= "<p>Der er ikke registreret medlemskab af eksterne organisationer for din bruger.</p>";
    }

    my $addHtml = qq'<select name="type">';
    my $orgs = allExternalOrgs();
    my $canAdd = 0;
    for my $k (sort keys %$orgs) {
        unless ($currentIDs{$k}) {
            my $o = $orgs->{$k};
            $addHtml .= qq'<option value="$o->{id}">$o->{name}</option>';
            $canAdd++;
        }
    }
    $addHtml .= qq'</select>';
    $addHtml .= qq' <button type="submit" name="add" value="1">Tilføj</button>';
    $html .= $addHtml if $canAdd;

    $html .= qq'</form>';

    return outputAccountPage('external', 'Medlemskaber', $html);
}

sub externalAddPage {
    my ($r, $q, $p) = @_;
    my $html = '';

    my $orgs = allExternalOrgs();
    my $org = $orgs->{$p->{type}} or die "Bad type: $p->{type}";
    my $errors = 0;

    $html .= qq'<form method="POST" action="/hal/account/external/edit">';
    $html .= qq'<input type="hidden" name="add" value="2">';
    $html .= qq'<input type="hidden" name="type" value="$p->{type}">';
    $html .= textInput($org->{field}, $org->{description}, 'value', $p, sub {
        my ($v, $p, $name) = @_;
        if ($v !~ /^\d{2,10}$/) {
            $errors++;
            return "Det skal være mindst 2 tal langt";
        }

        my $res = db->sql("select count(*) from external_member_id where external_organization_id=? and value=?",
            $org->{id}, $v);
        my ($inuse) = $res->fetchrow_array;
        $res->finish;

        if ($inuse) {
            $errors++;
            return "Allerede i brug, vælg et andet.";
        }
        return "";
    });

    $html .= qq'<input type="submit" name="doit" value="Gem">';
    $html .= qq'</form>';

    if ($p->{doit} && !$errors) {
        db->sql("insert into external_member_id (external_organization_id, member_id, value) values (?,?,?)",
            $org->{id}, getSession->{member_id}, $p->{value});
        return outputGoto("/hal/account/external");
    }

    return outputAccountPage('external/edit', 'Tilføj Medlemskab', $html);
}

sub externalDeletePage {
    my ($r, $q, $p) = @_;
    db->sql("delete from external_member_id where external_organization_id=? and member_id=?",
        $p->{delete}, getSession->{member_id});
    return outputGoto("/hal/account/external");
}

sub externalEditPage {
    my ($r, $q, $p) = @_;

    if ($p->{add}) {
        return externalAddPage($r, $q, $p);
    } elsif ($p->{delete}) {
        return externalDeletePage($r, $q, $p);
    } else {
        return outputGoto("/hal/account/external");
    }
}

BEGIN {
    addHandler(qr'^/hal/account/external$', \&externalPage);
    addHandler(qr'^/hal/account/external/edit$', \&externalEditPage);
}

42;
